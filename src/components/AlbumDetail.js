import React from 'react';
import { Image, Linking, Text, View }  from 'react-native';
import Button from './Button';
import Card from './Card';
import CardSection from './CardSection';

export default AlbumDetail = ({ album }) => {
    const { 
        artist, 
        image,
        title, 
        thumbnail_image, 
        url } = album;
    const { 
        bodyImageStyle,
        headerContentStyle, 
        headerTextStyle,
        headerThumbnailStyle, 
        headerThumbnailContainerStyle } = styles;

    return (
        <Card>
            <CardSection>
                <View style={ headerThumbnailContainerStyle }>
                    <Image 
                        style={ headerThumbnailStyle }
                        source={ {uri: thumbnail_image} } />
                </View>
                <View style={ headerContentStyle }>
                    <Text style={ headerTextStyle }>{ title }</Text>
                    <Text>{ artist }</Text>
                </View>                
            </CardSection>

            <CardSection>
                <Image 
                    style={ bodyImageStyle }
                    source={{uri: image}} />
            </CardSection>

            <CardSection>
                <Button onPress={() => Linking.openURL(url) }>
                    Buy Now!
                </Button>
            </CardSection>
        </Card>
    )
}

const styles = {
    headerContentStyle: {
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    headerTextStyle: {
        fontSize: 18
    },
    headerThumbnailContainerStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 10,
        marginRight: 10
    },
    headerThumbnailStyle: {
        height: 50,
        width: 50
    },
    bodyImageStyle: {
        flex: 1,
        height: 300,
        width: null
    }
}