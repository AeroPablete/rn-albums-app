import React, { Component } from 'react';
import { Text, View } from 'react-native';
import AlbumList from './components/AlbumList';
import Header from './components/Header';

export default class App extends Component {

    render() {
        return (
            <View style={{flex: 1, marginBottom: 10}}>
                <Header headerText="Albums" />
                <AlbumList />
            </View>
        );
    }    
}